package com.rad.mehd.weather_charger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.rad.mehd.weather_charger.yahoo.YahooModel;

import cz.msebera.android.httpclient.Header;

import static java.lang.Double.parseDouble;

public class MainActivity extends AppCompatActivity {


    TextView location;
    TextView highesttemp;
    TextView sunset;
    TextView sunrise;
    TextView rain;
    TextView lowesttemp;
    TextView temp;
    TextView condition;
    TextView day;
    TextView date;
    TextView battery;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      location = (TextView) findViewById(R.id.location);
        highesttemp = (TextView) findViewById(R.id.highesttemp);
        sunset = (TextView) findViewById(R.id.sunset);
        sunrise = (TextView) findViewById(R.id.sunrise);
        rain = (TextView) findViewById(R.id.rain);
        lowesttemp = (TextView) findViewById(R.id.lowesttemp);
        temp = (TextView) findViewById(R.id.temp);
        condition = (TextView) findViewById(R.id.condition);
        day = (TextView) findViewById(R.id.day);
        date = (TextView) findViewById(R.id.date);
        battery = (TextView) findViewById(R.id.battery);






        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22tehran%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";



        getdata(url);



}



    private void getdata(String url) {
        AsyncHttpClient clinet = new AsyncHttpClient();

        clinet.get(url, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(MainActivity.this, "conection error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parsedata(responseString);

            }
        });


    }

    private void parsedata(String responseString) {

        Gson gson = new Gson();
        YahooModel yahoo = gson.fromJson(responseString, YahooModel.class);
        String Sunrise = yahoo.getQuery().getResults().getChannel().getAstronomy().getSunrise();
        String Sunset = yahoo.getQuery().getResults().getChannel().getAstronomy().getSunset();
        String city = yahoo.getQuery().getResults().getChannel().getLocation().getCity();
        String Windspeed = yahoo.getQuery().getResults().getChannel().getWind().getSpeed();
        String Temp = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        String rising = yahoo.getQuery().getResults().getChannel().getAtmosphere().getRising();
        String hightemp = yahoo.getQuery().getResults().getChannel().getItem().getForecast().get(0).getHigh();
        String lowtemp = yahoo.getQuery().getResults().getChannel().getItem().getForecast().get(0).getLow();
        String condith = yahoo.getQuery().getResults().getChannel().getItem().getForecast().get(0).getText();
        String Day = yahoo.getQuery().getResults().getChannel().getItem().getForecast().get(0).getDay();
        String Date = yahoo.getQuery().getResults().getChannel().getItem().getForecast().get(0).getDate();



        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        int bb = batLevel;

        final String batt = String.valueOf(bb);



        Double c = parseDouble(Temp);
        c = (c-32)/1.8;
        String cTemp = String.format("%.2f",c);

        String finaltemp = cTemp + "°C";

        String finalwindspeed = Windspeed + " mph";

        Double hc = parseDouble(hightemp);
        hc = (hc-32)/1.8;
        String chTemp = String.format("%.1f",hc);

        String finalhtemp = chTemp + "°";


        Double lc = parseDouble(lowtemp);
        lc = (lc-32)/1.8;
        String clTemp = String.format("%.1f",lc);

        String finalltemp = clTemp + "°";





        highesttemp.setText(finalhtemp);
        lowesttemp.setText(finalltemp);
        sunrise.setText(Sunrise);
        sunset.setText(Sunset);
        location.setText(city);
        rain.setText(rising + "%");
        temp.setText(finaltemp);
        condition.setText(condith);
        date.setText(Date);
        day.setText(Day);
        battery.setText(batt);




    }


}
