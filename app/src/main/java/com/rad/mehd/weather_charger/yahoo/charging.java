package com.rad.mehd.weather_charger.yahoo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.rad.mehd.weather_charger.MainActivity;

import static android.R.attr.start;

/**
 * Created by mehD on 8/11/2017.
 */

public class charging extends BroadcastReceiver {

    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Battery is charging", Toast.LENGTH_SHORT).show();

        Intent newintent = new Intent(context, MainActivity.class);
        context.startActivity(newintent);


    }
}
